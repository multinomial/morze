to_morse = {'A': '.−', 'B': '−...', 'C': '−.−.', 'D': '−..', 'E': '.', 'F': '..−.', 'G': '−−.', 'H': '....', 'I': '..', 'J': '.−−−', 'K': '−.−', 'L': '.−..', 'M': '−−', 'N': '−.', 'O': '−−−', 'P': '.−−.', 'Q': '−−.−', 'R': '.−.', 'S': '...', 'T': '−', 'U': '..−', 'V': '...−', 'W': '.−−', 'X': '−..−', 'Y': '−.−−', 'Z': '−−..', '1': '.−−−−', '2': '..−−−', '3': '...−−', '4': '....−', '5': '.....', '6': '−....', '7': '−−...', '8': '−−−..', '9': '−−−−.', '0': '−−−−−', ', ': '−−..−−', '.': '.−.−.−', '?': '..−−..', '/': '−..−.', '-': '−....−', '(': '−.−−.', ')': '−.−−.−'}

from_morse = dict([(value, key) for key, value in to_morse.items()]) 

def code_morse(string):
    translated = [to_morse[key] for key in string.upper().strip().replace(" ", "")]
    return " ".join(translated)
